<?php

/**
 * Implements hook_drush_command().
 */
function drush_fui_drush_command($feature = NULL) {

  $items['drush-fui-command'] = array(
    'description' => 'Shortcut for drush feature-update --version-increment.
WARNING: Automatically adds --yes argument!',
    'aliases' => array('fui'),
    'arguments' => array(
      'feature' => 'Name of the feature to be updated.',
    ),
    'examples' => array(
      'drush fui feature_name' => 'Updates feature_name and increases the version number.',
    ),
  );

  return $items;
}

function drush_drush_fui_command($feature = NULL) {
  drush_fui_print_statement($feature);
}
